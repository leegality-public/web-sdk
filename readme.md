# Leegality Web SDK 6.0#

### Steps to use: ###

1. Download Leegality Library from https://gitlab.com/leegality-public/web-sdk/-/blob/master/leegalityv6.js
2. Insert the SDK JS file in head tag of HTML.
    <br __>Ex. `<script src="leegalityv6.js" type="text/javascript"></script>`
3. Create a JSON object with following keys:
    <br __>3.1 logoUrl: (This is an optional parameter) This key contains a URL of your logo in any image file format. This Logo is shown on the Loading Page before the Signing Page appears. 
    <br __>3.2 callback: (This is a required parameter) This key contains a function which will be automatically executed after the completion of signing or in case of any error.
    <br __>Ex. `var obj = {
	    logoUrl: "your-logo-url",
	    callback: function(response) {
		      	console.log(resposne);
		      }
	}`
4. To initialize signing, create a Leegality instance provided by leegality SDK by passing object created in step 3.
    <br __>Ex. `var leegality = new Leegality(obj);`
5. Call method leegality.init() on user’s click/action. 
    <br __>Ex. `leegality.init();`
6. After calling the init() method, call method eSign with the signing URL.
    <br __>Ex. `var signingUrl = “your-signing-url”;`
    <br __>`leegality.esign(signingUrl);`

    To customise the duration for which the success screen on the URL remains open before automatically closing down, you can append the URL with the
    query parameter “timer”.  The default value is 5 and you can pass any integer value between 0 to 60.  For example, to set the duration at 0
    seconds, pass the timer parameter with value 0 as follows:

    https://app1.leegality.com/sign/ea23fced-36e4-4fe3-ae32-181a09c2f13a?timer=0

7. At any time,  if you want to cancel signing process, call the method cancel() on instance of Leegality class.
    <br __>Ex. `leegality.cancel();`


### Callback: ###

1. In case of successful completion of signing process, signing popup will be closed automatically and you will get a response object with key name 'message' - with corresponding 'success message' and key name ‘documentId’ - with corresponding Document ID.
    <br __>Ex. `{"message": "success message", “documentId”: “Document ID”}`
2. In case of error in signing process, signing popup will be closed automatically and you will get a response object with key name 'error' and corresponding 'error message'.
    <br __>Ex. `{"error": "error message"}`


### Sample: ###

```
<html>
    <head>
        <script src="leegalityv5.js" type="text/javascript"></script>
    </head>
    <body>
        <p>Message: <span id="message"></span></p>
        <a href="javascript:void(0);" onclick="sign()">Start Signing</a>
        <script>
            function callback(response) {
            	if(response.error){
            		document.getElementById('message').style.color = "red";
            		document.getElementById('message').innerHTML = response.error;
            	}else{
            		document.getElementById('message').style.color = "green";
            		document.getElementById('message').innerHTML = response.message;
            	}
            }
            var obj = {
            	    logoUrl: "your-logo-url",
            	    callback: callback	
                };
            function sign(){
            	var leegality = new Leegality(obj);
                leegality.init();
                //You will get a signing url by uploading document from backend.
            	var signingUrl = “your-signing-url”;
            	leegality.esign(signingUrl);
            }
        </script>
    </body>
</html>
```
